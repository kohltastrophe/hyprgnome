#!/usr/bin/env python3

import configparser
import threading
import time
import subprocess
import sys
import re
import os
import gi
gi.require_version("Gtk", "4.0")
gi.require_version("Adw", "1")
from gi.repository import Adw, Gtk, Gdk, Gio, GLib, Pango

from debounce import debounce

# update exchange rate
os.system("qalc -e &")

css_provider = Gtk.CssProvider()
css_provider.load_from_path(os.environ.get('HOME') + '/.config/hypr/css/launcher.css')
Gtk.StyleContext.add_provider_for_display(Gdk.Display.get_default(), css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

def get_app_keywords(info):
    desktop_file_path = info.get_filename()
    config = configparser.ConfigParser()
    config.read(desktop_file_path)

    if "Desktop Entry" in config and "Keywords" in config["Desktop Entry"]:
        return config["Desktop Entry"]["Keywords"].split(";")
    else:
        return [] # No keywords found

def app_button(info):
    # Create a box to hold the icon and label vertically
    box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, valign=Gtk.Align.CENTER)

    frame = Gtk.Frame(child=box)
    frame.add_css_class("app")
    frame.set_size_request(128, 128)

    if info.icon:
        image = Gtk.Image.new_from_gicon(info.icon)
        image.set_pixel_size(64)
        box.append(image)

    label = Gtk.Label(
        use_markup=True,
        label=info.label_text,
        tooltip_markup=info.tooltip,
        ellipsize=Pango.EllipsizeMode.END,
        justify=Gtk.Justification.CENTER,
        max_width_chars=0,
        lines=2,
    )
    box.append(label)
    frame.info = info

    return frame

class MainWindow(Adw.ApplicationWindow):
    def __init__(self, **kwargs):
        self.last_query = ""
        self.load_app_info()
        super().__init__(**kwargs)

        # Box for content
        view_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, css_name="#content")
        self.set_content(view_box)

        # Search Bar
        self.search_bar = Gtk.SearchEntry(placeholder_text="Search...", search_delay=30, halign=Gtk.Align.CENTER)
        self.search_bar.set_size_request(512, -1)
        self.search_bar.set_key_capture_widget(self)
        self.search_bar.connect("search-changed", self.on_search_changed)
        self.search_bar.connect("activate", self.activate_selected_child)
        view_box.append(self.search_bar)

        # Scrolled Window for Applications
        self.scrolled_window = Gtk.ScrolledWindow(vexpand=True, css_name="#scroll")
        view_box.append(self.scrolled_window)

        self.result_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, halign=Gtk.Align.CENTER)

        # Flow Box to Organize Applications
        self.flowbox = Gtk.FlowBox(
            activate_on_single_click=True,
            homogeneous=True,
            max_children_per_line=8,
            selection_mode=Gtk.SelectionMode.SINGLE,
            halign=Gtk.Align.CENTER,
        )
        self.flowbox.connect("child-activated", self.launch_app)
        self.flowbox.connect("move-cursor", self.on_move_cursor)
        self.flowbox.connect("selected-children-changed", lambda flow: len(flow.get_selected_children()) > 0 and self.search_list.unselect_all())
        self.result_box.append(self.flowbox)

        # Filtered list for other search optionsF
        self.search_list = Gtk.ListBox(
            activate_on_single_click=True,
            selection_mode=Gtk.SelectionMode.SINGLE,
        )
        self.search_list.connect("move-cursor", self.on_move_cursor)
        self.search_list.connect("row-activated", lambda _, row: row.get_child().search_action())
        self.search_list.connect("row-selected", lambda _, row: row and self.flowbox.unselect_all())
        self.result_box.append(self.search_list)

        self.scrolled_window.set_child(self.result_box)
        self.connect("notify::is-active", self.on_active_changed)

    def on_active_changed(self, window, gparam):
        result = subprocess.run("hyprctl monitors -j | jq -r '.[] | .specialWorkspace.name'", capture_output=True, shell=True, text=True)
        visible = bool(result.returncode == 0 and result.stdout.strip() == "special:launcher")
        if not visible:
            if window.is_active():
                if time.time() > self.next_load:
                    self.load_app_info()
            else:
                self.reset_state()

    def reset_state(self):
        self.flowbox.unselect_all()
        self.search_list.unselect_all()
        self.search_bar.set_text("")
        self.search_bar.grab_focus()

    def launch_app(self, flowbox, flowboxChild):
        os.system("hyprctl dispatch togglespecialworkspace launcher")
        flowboxChild.get_child().info.launch([], None)
        self.reset_state()

    def view_all(self):
        self.flowbox.remove_all()
        for info in self.app_info:
            self.flowbox.append(info.button)
        self.flowbox.set_max_children_per_line(8)

    def activate_selected_child(self, search_entry):
        selected_child = self.flowbox.get_selected_children()
        if selected_child:
            return self.launch_app(self.flowbox, selected_child[0])
        selected_child = self.search_list.get_selected_row()
        if selected_child:
            selected_child.activate()
    
    def on_link_clicked(self, label, uri):
        # TODO scroll back to topscroll
        self.flowbox.unselect_all()
        self.search_list.unselect_all()
        self.flowbox.remove_all()
        self.search_list.remove_all()
        self.scrolled_window.get_vadjustment().set_value(0)
        self.dictionaryQuery(uri)

    def on_move_cursor(self, widget, step, count, extend, modify):
        if (extend or modify) or step != Gtk.MovementStep.DISPLAY_LINES:
            return
        if widget == self.flowbox:
            items = widget.get_selected_children()
            if items:
                item = items[0]
                index = item.get_index()
                lastIndex = widget.get_last_child().get_index()
                margin = item.get_style_context().get_margin()
                columns = widget.get_width() / (item.get_width() + margin.left + margin.right)
                if count == -1 and index + 1 <= columns: # move up
                    self.search_bar.grab_focus()
                elif count == 1 and index >= lastIndex - columns and self.search_list.get_visible():
                    row = self.search_list.get_row_at_index(0)
                    if row:
                        row.grab_focus()
                        self.search_list.select_row(row)
        elif count == -1 and widget == self.search_list and widget.get_selected_row() == widget.get_first_child():
            widget.do_move_focus(widget, Gtk.DirectionType.TAB_BACKWARD)

    def dictionaryQuery(self, query):
        # TODO make it update the text instead with a simple placeholder after debounce
        result = subprocess.run(["dict", "-d", "gcide", query], capture_output=True, text=True)
        if result.returncode == 0 and result.stdout:
            lines = result.stdout.splitlines()[4:] # Remove the first 4 lines
            paragraph_lines = []
            for line in lines:
                if line.strip():     # Check if the line is not empty (i.e., not just whitespace)
                    paragraph_lines.append(line)
                else:
                    break           # Stop when we reach a blank line (end of the paragraph)
            definition = "\n".join(lines)
            # Regex pattern for \... and {...} sections
            formatted_text = re.sub(r"\\(.*?)\\|\{(.*?)\}", lambda m:
            f"<i><span foreground='gray'>{m.group(1)}</span></i>" if m.group(1) else
            f"<a href='{m.group(2)}'>{m.group(2)}</a>", 
            GLib.markup_escape_text(definition))
            self.search_item(
                "dictionary",
                formatted_text,
                lambda: self.reset_state(),
                Gtk.Justification.LEFT
            )
    
    @debounce(0.2)
    def debounceDictionary(self, query):
        self.dictionaryQuery(query)

    def on_search_changed(self, search_entry):
        rawquery = search_entry.get_text()
        query = rawquery.lower()
        if query == self.last_query:
            return
        self.last_query = query
        self.search_list.set_visible(query)
        # Repopulate flowbox with all applications
        if not query:
            self.flowbox.unselect_all()
            self.search_list.unselect_all()
            return self.view_all()
        # Search query
        self.flowbox.remove_all()
        self.search_list.remove_all()

        # math expressions, ignore single alphanumeric word query
        if not bool(re.fullmatch(r"^[\w]*$", query)):
            result = subprocess.run(["qalc", "-t", query + " "], capture_output=True, text=True)
            resultText = result.stdout.strip()
            if result.returncode == 0 and query != resultText.lower():
                self.search_item(
                    "gnome-calculator",
                    f"<span fgalpha='60%'>{GLib.markup_escape_text(rawquery)} =</span>\n"
                    + f"<span font='24'><b>{GLib.markup_escape_text(resultText)}</b></span>\n"
                    + "<span fgalpha='30%'><i>Copy to clipboard</i></span>",
                    lambda: (os.system("hyprctl dispatch togglespecialworkspace launcher; wl-copy " + resultText), self.reset_state())
                )

        # TODO dictionary/thesauraus entries!!!
        self.debounceDictionary(query)

        # Select first search list row
        # it will be unselected if there's an application match
        row = self.search_list.get_row_at_index(0)
        if row:
            self.search_list.select_row(row)

        prefix_name = []
        prefix_keyword = []
        contains = []
        # Prefix match name
        for info in self.app_info:
            if info.name_lower.startswith(query):
                prefix_name.append(info)
                continue
        # Prefix match keyword
            if any(tag.startswith(query) for tag in info.keywords):
                prefix_keyword.append(info)
                continue
        # Contains match
            if query in info.name_lower:
                contains.append(info)
            elif any(query in tag for tag in info.keywords):
                contains.append(info)
        # Populate filtered applications
        filtered_apps = prefix_name + prefix_keyword + contains
        for info in filtered_apps:
            self.flowbox.append(app_button(info))
        # Focus first child
        if len(filtered_apps) > 0:
            first_child = self.flowbox.get_first_child()
            if first_child:
                self.search_list.unselect_all()
                self.flowbox.select_child(first_child)
            self.flowbox.set_max_children_per_line(min(8, len(filtered_apps)))
        self.search_list.set_size_request(512 if self.search_list.get_first_child() else -1, -1)

    def search_item(self, icon, text, action, justify=Gtk.Justification.RIGHT):
        box = Gtk.Box(hexpand=True, vexpand=True, valign=Gtk.Align.CENTER)

        frame = Gtk.Frame(child=box)
        frame.add_css_class("listitem")
        frame.set_size_request(512, -1)

        image = Gtk.Image.new_from_icon_name(icon)
        image.set_valign(Gtk.Align.START)
        image.set_pixel_size(64)
        box.append(image)

        # spacer for right aligned text
        if justify == Gtk.Justification.RIGHT:
            box.append(Gtk.Box(hexpand=True))

        label = Gtk.Label(
            use_markup=True,
            label=text,
            ellipsize=Pango.EllipsizeMode.END,
            justify=justify,
            valign=Gtk.Align.CENTER,
            yalign=0,
        )
        label.connect("activate-link", self.on_link_clicked)
        box.append(label)
        frame.search_action = action
        self.search_list.append(frame)
        return frame

    def load_app_info(self):
        self.next_load = time.time() + 5
        def _load_app_info_thread():
            self.app_info = sorted(
                [info for info in Gio.AppInfo.get_all() if info.should_show()],
                key=lambda info: info.get_display_name().lower(),
            )

            for info in self.app_info:
                info.icon = info.get_icon()
                display_name = info.get_display_name()
                escaped_name = GLib.markup_escape_text(display_name)
                info.name_lower = display_name.lower()
                info.keywords = get_app_keywords(info)
                info.label_text = f"<b>{escaped_name}</b>"
                info.tooltip = f"<b>{escaped_name}</b>{'\n' if info.keywords else ''}<i>{GLib.markup_escape_text(' • '.join(info.keywords)[:-3])}</i>"
                info.button = app_button(info)
                info.keywords = [keyword.lower() for keyword in info.keywords]

            GLib.idle_add(lambda: self.view_all())

        threading.Thread(target=_load_app_info_thread, daemon=True).start()

class MyApp(Adw.Application):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.connect('activate', self.on_activate)

    def on_activate(self, app):
        self.win = MainWindow(application=app, title="Hyprgnome Launcher")
        self.win.present()

app = MyApp(application_id="hyprgnome.launcher")
app.run(sys.argv)
