#!/usr/bin/env bash
name=$(hyprctl monitors -j | jq -r '.[] | select(.focused == true) | .specialWorkspace.name')
if [[ $name != 'special:switcher' ]]; then
	hyprctl dispatch togglespecialworkspace switcher
fi
