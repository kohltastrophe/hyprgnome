#!/usr/bin/env bash

# define 5 workspaces per monitor by id
HYPRRULES=~/.config/hypr/source/pre/workspace-rules.conf
HYPRMONITORS=$(hyprctl -j monitors)
HYPRCFG=$(echo $HYPRMONITORS | jq -j '.[] | [ "workspace = \(.id*5+1), monitor:\(.name), default:true, persistent:true", [range(2; 6)] as $wsnums | "workspace = \($wsnums[] + .id*5), monitor:\(.name), persistent:true" ] | join("\n") + "\n"')

echo "$HYPRCFG" >$HYPRRULES
hyprctl -r reload
# open every workspace once
HYPRCMDS=$(echo $HYPRMONITORS | jq -j '.[] | [range(1; 6)] as $wsnums | "dispatch workspace \($wsnums[] + .id*5);\n"')
# finally change back to default workspaces
HYPRCMDS+=$(echo $HYPRMONITORS | jq -j '.[] | " dispatch workspace \(.id*5+1);"')
hyprctl --batch "$HYPRCMDS"
# make sure workspaces are on the correct monitor
HYPRCMDS=$(echo $HYPRMONITORS | jq -j '.[] | [range(1; 6)] as $wsnums | "dispatch moveworkspacetomonitor \($wsnums[] + .id*5) \(.name);\n"')
hyprctl --batch "$HYPRCMDS"

# TODO: handle monitor changes
