#!/usr/bin/env python3
import gi
import os
import json
import time

gi.require_version("Gtk", "4.0")
gi.require_version("Adw", "1")
from gi.repository import Adw, Gtk, Gdk, Gio, GLib, Pango

css_provider = Gtk.CssProvider()
css_provider.load_from_path(os.environ.get('HOME') + '/.config/hypr/css/launcher.css')
Gtk.StyleContext.add_provider_for_display(Gdk.Display.get_default(), css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

def window_button(w):
    # Create a box to hold the icon and label vertically
    box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, valign=Gtk.Align.CENTER)

    frame = Gtk.Frame(child=box)
    frame.add_css_class("app")
    frame.add_css_class("switcher")
    frame.set_size_request(128, 128)

    if w["icon"]:
        image = Gtk.Image.new_from_paintable(w["icon"])
        image.set_pixel_size(64)
        box.append(image)

    title = GLib.markup_escape_text(w["title"])

    label = Gtk.Label(
        use_markup=True,
        label=f"<b>{title}</b>",
        tooltip_markup="<b>%s</b> (%s)"
        % (title, GLib.markup_escape_text(str(w["workspace"]["id"]))),
        ellipsize=Pango.EllipsizeMode.END,
        justify=Gtk.Justification.CENTER,
        max_width_chars=0,
    )
    box.append(label)
    frame.info = w

    return frame

class MainWindow(Adw.ApplicationWindow):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.add_css_class("switcher")
        self.iconTheme = Gtk.IconTheme.get_for_display(Gdk.Display.get_default())

        self.key_controller = Gtk.EventControllerKey()
        self.key_controller.connect("key-pressed", self.on_key_pressed)
        self.key_controller.connect("key-released", self.on_key_released)

        # Flow Box to Organize Windows
        self.flowbox = Gtk.FlowBox(
            activate_on_single_click=True,
            homogeneous=True,
            max_children_per_line=1,
            selection_mode=Gtk.SelectionMode.SINGLE,
            valign=Gtk.Align.CENTER,
            halign=Gtk.Align.CENTER,
            orientation=Gtk.Orientation.VERTICAL,
        )
        self.flowbox.add_css_class("switcher")
        self.flowbox.connect("child-activated", lambda _,__: self.focus_window())
        self.flowbox.add_controller(self.key_controller)
        self.set_content(self.flowbox)

        self.connect("notify::is-active", self.on_active_changed)
        # initial loading of window entries into the flowbox
        self.load_windows()

    def on_key_pressed(self, controller, keyval, keycode, state):
        if keyval == Gdk.KEY_ISO_Left_Tab and state & (Gdk.ModifierType.SHIFT_MASK | Gdk.ModifierType.ALT_MASK):
            success = self.flowbox.child_focus(Gtk.DirectionType.TAB_BACKWARD)
            if not success:
                child = self.flowbox.get_last_child()
                self.flowbox.select_child(child)
                child.grab_focus()
        elif keyval == Gdk.KEY_Tab and state & Gdk.ModifierType.ALT_MASK:
            success = self.flowbox.child_focus(Gtk.DirectionType.TAB_FORWARD)
            if not success:
                child = self.flowbox.get_first_child()
                self.flowbox.select_child(child)
                child.grab_focus()

    def on_key_released(self, controller, keyval, keycode, state):
        if keyval == Gdk.KEY_Alt_L or keyval == Gdk.KEY_Alt_R:
            self.focus_window()

    def on_active_changed(self, window, gparam):
        if window.is_active():
            self.flowbox.remove_all()
            self.load_windows()
        else:
            self.flowbox.remove_all()

    def focus_window(self):
        os.system("hyprctl dispatch togglespecialworkspace switcher")
        os.system(f"hyprctl dispatch focuswindow address:{self.flowbox.get_selected_children()[0].get_child().info["address"]}")
        self.flowbox.unselect_all()

    def load_windows(self):
        windows = json.loads(os.popen("hyprctl -j clients").read())
        filtered_windows = list(
            filter(
                lambda w: w["workspace"]["id"] != -1
                and not w["workspace"]["name"].startswith("special:"),
                windows,
            )
        )
        filtered_windows.sort(key=lambda w: w["focusHistoryID"])

        for w in filtered_windows:
            wClass = "discord" if w["class"] == "vesktop" else w["class"]

            w["icon"] = self.iconTheme.lookup_icon(
                wClass,
                w["title"] + ", gnome-settings-theme",
                64,
                1,
                Gtk.TextDirection.LTR,
                Gtk.IconLookupFlags.PRELOAD,
            )
            frame = window_button(w)
            self.flowbox.append(frame)
        child = self.flowbox.get_first_child()
        if len(filtered_windows) > 1:
            child = self.flowbox.get_child_at_index(1)
        self.flowbox.select_child(child)
        child.grab_focus()


class MyApp(Adw.Application):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.connect("activate", self.on_activate)

    def on_activate(self, app):
        self.win = MainWindow(application=app, title="Hyprgnome Window Switcher")
        self.win.present()


app = MyApp(application_id="hyprgnome.switcher")
app.run()
