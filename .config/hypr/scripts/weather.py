#!/usr/bin/env python3

import time
import gi
gi.require_version('GWeather', '4.0')
from gi.repository import Gio, GLib, GWeather

settings = Gio.Settings(schema_id="org.gnome.Weather")
locations = settings.get_value('locations')
location = locations.get_child_value(0).get_child_value(0)

world = GWeather.Location.get_world()
glocation = world.deserialize(location)

print(locations)
print(location)
print(glocation)
if not glocation:
	exit()

print(glocation.get_name(), glocation.get_country_name(), glocation.get_level())

def on_update(info):
	print(info.get_weather_summary())
	current_condition = info.get_conditions()
	_, temperature = info.get_value_temp(GWeather.TemperatureUnit.DEFAULT)
	_, feels_like = info.get_value_apparent(GWeather.TemperatureUnit.DEFAULT)
	print(f"Current: {current_condition}\nTemperature: {temperature:.1f}°C")

try:
	info = GWeather.Info(
		application_id = "org.gnome.Weather",
		contact_info = "https://gitlab.gnome.org/GNOME/gnome-weather/-/raw/master/gnome-weather.doap",
		enabled_providers = (GWeather.Provider.METAR | GWeather.Provider.MET_NO | GWeather.Provider.OWM),
		location = glocation )
	info.connect("::updated", on_update)
	info.update()

	time.sleep(5)

	print(info.get_update())

	current_condition = info.get_conditions()
	_, temperature = info.get_value_temp(GWeather.TemperatureUnit.DEFAULT)
	_, feels_like = info.get_value_apparent(GWeather.TemperatureUnit.DEFAULT)
	print(f"Current: {current_condition}\nTemperature: {temperature:.1f}°C")

except GLib.Error as e:
	print(f"Weather Error: {e.message}")
