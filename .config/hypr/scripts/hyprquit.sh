#!/usr/bin/env bash

function no_clients(){
	[ "$(hyprctl clients | grep "class:" | wc -l)" -eq "0" ]
}

function close_apps() {
	if no_clients; then
		return
	fi

	notify-send "power controls" "Closing applications..."

	# close gemini first to make sure it doesn't save in the chrome session
	hyprctl dispatch closewindow "class:(assistant.google.com__tasks)";
	hyprctl dispatch closewindow "class:(gemini.google.com)";
	hyprctl dispatch closewindow "class:(org.gnome.Calendar)";
	hyprctl dispatch closewindow "class:(org.gnome.Weather)";
	sleep 0.5;

	local timeout=10  # Timeout in seconds
	local interval=1  # Check interval in seconds

	while [ "$timeout" -gt 0 ]; do
		if no_clients; then
				return
		fi

		if ["$timeout" -lt 10]; then
			notify-send "power controls" "One or more applications are still open. Shutting down in $timeout seconds..."
		fi

		# close all client windows
		# required for graceful exit since many apps aren't good SIGNAL citizens
		local cmds=$(hyprctl -j clients | jq -j '.[] | "dispatch closewindow address:\(.address); "')
		hyprctl --batch "$cmds" >> /tmp/hyprexitwithgrace.log 2>&1;

		sleep "$interval"
		timeout=$((timeout - interval))
	done
}

case "$1" in
	shutdown)
		zenity --question --title "Shutdown" --icon system-shutdown-symbolic || exit 1
		close_apps
		systemctl poweroff
	;;

	reboot | restart)
		zenity --question --title "Restart" --icon system-restart-symbolic || exit 1
		close_apps
		systemctl reboot
	;;

	suspend)
		zenity --question --title "Sleep" --icon system-suspend-symbolic || exit 1
		hyprlock
		sleep 3
		systemctl suspend
	;;

	logout)
		zenity --question --title "Logout" --icon system-log-out-symbolic || exit 1
		close_apps
		notify-send "power controls" "Logging out."
		hyprctl dispatch exit
	;;

	lock)
		hyprlock
	;;

	close)
		close_apps
	;;

	*)
		echo $"Usage: $0 {shutdown|reboot|suspend|logout|lock|close}"
		exit 1
esac