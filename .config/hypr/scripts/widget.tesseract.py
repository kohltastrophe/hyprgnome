#!/usr/bin/env python3
import subprocess
import sys
import tempfile
import gi

gi.require_version("Gtk", "4.0")
gi.require_version("Adw", "1")
from gi.repository import Adw, Gio, GLib, Gtk


# TODO clean up old tmp files that weren't properly closed
image_data = sys.stdin.buffer.read()
temp_image = tempfile.NamedTemporaryFile(delete=False, suffix=".png")
temp_image.write(image_data)
temp_image.flush()
# TODO implement magick deskew for OSD orientation
scaled_image = tempfile.NamedTemporaryFile(delete=False, suffix=".png")
subprocess.run(
    ["convert", "-density", "300", temp_image.name, scaled_image.name], check=True
)


class OCRWindow(Adw.ApplicationWindow):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # Content layout
        page = Adw.PreferencesPage()
        self.set_content(page)

        # Image Preview Group
        preview_group = Adw.PreferencesGroup(title="Image Preview")

        # Using Gtk.Picture for the preview
        file = Gio.File.new_for_path(temp_image.name)
        self.image_preview = Gtk.Picture()
        self.image_preview.set_file(file)
        self.image_preview.set_can_shrink(False)
        preview_group.add(self.image_preview)
        page.add(preview_group)

        # OCR Output Group
        output_group = Adw.PreferencesGroup(title="OCR Output")

        # OCR output
        self.textview = Gtk.TextView(editable=False)
        output_group.add(self.textview)
        page.add(output_group)

        # Tesseract Options Group
        options_group = Adw.PreferencesGroup(title="Tesseract Options")

        # Use Dictionary Checkbox
        self.use_dict_switch = Gtk.CheckButton(
            valign=Gtk.Align.CENTER, label="Use Dictionary"
        )
        self.use_dict_switch.set_active(True)
        options_group.add(self.use_dict_switch)

        # Page Segmentation Mode Options
        psm_options = [
            (0, "OSD"),
            (1, "APS with OSD"),
            (3, "APS no OSD (Default)"),
            (4, "Column"),
            (5, "Block rotated 90°"),
            (6, "Block"),
            (7, "Line"),
            (8, "Word"),
            (9, "Word circular path"),
            (10, "Character"),
            (11, "Sparse text unordered"),
            (12, "Sparse text with OSD"),
            (13, "Raw line, bypasses hacks"),
        ]

        psm_model = Gtk.StringList()
        for value, description in psm_options:
            psm_model.append(f"{str(value)}\t{description}")

        # Create combo row and set its model
        self.psm_combo = Adw.ComboRow(title="Page Segmentation Mode")
        self.psm_combo.set_model(psm_model)
        self.psm_combo.set_selected(2)
        # Add combo row to the options group
        options_group.add(self.psm_combo)

        page.add(options_group)

        # Run OCR on Initialization
        self.on_run_ocr()

        # Listen to option changes
        self.psm_combo.connect("notify::selected", lambda _, __: self.on_run_ocr())
        self.use_dict_switch.connect("toggled", lambda _: self.on_run_ocr())

    def on_run_ocr(self):
        psm = self.psm_combo.get_selected_item().get_string()
        use_dict = self.use_dict_switch.get_active()

        if scaled_image:
            command = ["tesseract", "--dpi", "300", scaled_image.name, "stdout", "--psm", psm]
            if not use_dict:
                command.extend(["-c", "load_system_dawg=0", "-c", "load_freq_dawg=0"])
            try:
                process = subprocess.run(command, capture_output=True, text=True)
                if process.returncode == 0:
                    self.textview.get_buffer().set_text(process.stdout)
                else:
                    self.textview.get_buffer().set_text(
                        f"Error running Tesseract: {process.stderr}"
                    )
                    print(f"Error running Tesseract: {process.stderr}")
            except FileNotFoundError:
                self.textview.get_buffer().set_text(
                    "Tesseract not found. Please make sure it's installed."
                )
                print("Tesseract not found. Please make sure it's installed.")


app = Adw.Application(application_id="hyprgnome.widget.tesseract")
app.connect(
    "activate", lambda app: OCRWindow(application=app, title="Tesseract OCR").present()
)
app.run(None)

# After the application has finished, clean up the temp files
temp_image.close()
scaled_image.close()
