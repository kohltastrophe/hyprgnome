#!/usr/bin/env python

import json
import psutil
import subprocess
from collections import namedtuple

def get_icon_range(current, minv, maxv, icons):
    current = int(current)
    if current <= minv:
        return icons[0]
    elif current >= maxv:
        return icons[-1]
    else:
        return icons[int((current - minv) / (maxv - minv) * (len(icons) - 1))]

tempIcon = [
    "❄️",
    "",
    "",
    "",
    "",
    "",
    "",
    "🔥",
    "💥",
]

# NVIDIA sensors
GPUInfo = namedtuple("GPUInfo", ["current", "percent", "mem_percent", "mem_total", "mem_used", "mem_free"])
command = "nvidia-smi --query-gpu=temperature.gpu,utilization.gpu,utilization.memory,memory.total,memory.used,memory.free --format=csv,noheader,nounits"
result = subprocess.run(command, capture_output=True, shell=True, text=True)
gpu_info = None
if result.returncode == 0:
    output = result.stdout.strip()
    if output:
        gpu_info = GPUInfo(*output.split(", "))

# Get lm_sensors temperatures
cpu_info = None
temps = psutil.sensors_temperatures()
for name, entries in temps.items():
    for entry in entries:
        if entry.label == "CPU": cpu_info = entry

# CPU Usage and Memory
cpu_info_percent = int(psutil.cpu_percent())
mem_info = psutil.virtual_memory()

cpu_icon = get_icon_range(cpu_info.current, 15, 80, tempIcon)
label = f"CPU {cpu_icon} {int(cpu_info.current)}°C  {cpu_info_percent}% 󰍛 {int(mem_info.percent)}%"

if gpu_info:
    gpu_icon = get_icon_range(gpu_info.current, 15, 80, tempIcon)
    label += f" GPU {gpu_icon} {int(gpu_info.current)}°C  {int(gpu_info.percent)}% 󰍛 {int(gpu_info.mem_percent)}%"
else:
    gpu_info = {
        ["current"]: "?",
        ["percent"]: "?",
        ["mem_percent"]: "?",
        ["mem_total"]: "?",
        ["mem_used"]: "?",
        ["mem_free"]: "?",
    }

hot_icon = gpu_icon if gpu_info and int(gpu_info.current) > int(cpu_info.current) else cpu_icon
tooltip = "\t\t\t<big><b>CPU</b></big>\t\t<big><b>GPU</b></big>\t\n"
tooltip += f" {hot_icon} <b>Temperature</b>\t\t{cpu_info.current}°C\t\t{gpu_info.current}°C\n"
tooltip += f"  <b>Usage</b>\t\t{cpu_info_percent}%\t\t{int(gpu_info.percent)}%\n"
tooltip += f" 󰍛 <b>Memory</b>\t\t{int(mem_info.percent)}%\t\t{int(gpu_info.mem_percent)}%\n"
tooltip += f" 󰍛 <b>Memory Used</b>\t\t{mem_info.used / (1024 ** 3):.1f} GB\t\t{int(gpu_info.mem_used) / 1024:.1f} GB\n"
tooltip += f" 󰍛 <b>Memory Free</b>\t\t{mem_info.available / (1024 ** 3):.1f} GB\t\t{int(gpu_info.mem_free) / 1024:.1f} GB\n"
tooltip += f" 󰍛 <b>Memory Total</b>\t\t{mem_info.total / (1024 ** 3):.1f} GB\t\t{int(gpu_info.mem_total) / 1024:.1f} GB\n"

print(json.dumps({"text": label, "tooltip": tooltip}))
