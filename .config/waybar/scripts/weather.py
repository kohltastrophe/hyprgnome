#!/usr/bin/env python

import json
import requests
from datetime import datetime

WEATHER_CODES = {
	'113': ['', ''], # sunny clear
	'116': ['', ''], # part cloudy
	'119': ['', ''], # cloudy
	'122': ['', ''], # overcast
	'143': ['', ''],# mist
	'176': ['', ''],# patchy rain nearby
	'179': ['', ''],# patchy snow nearby
	'182': ['', ''],# patchy sleet nearby
	'185': ['', ''],# patchy freezing drizzle nearby
	'200': ['', ''],# thundery outbreaks in area
	'227': ['', ''],# blowing snow
	'230': ['', ''],# blizzard
	'248': ['', ''], # fog
	'260': ['', ''],# freezing fog
	'263': ['', ''],# patchy light drizzle
	'266': ['', ''],# light drizzle
	'281': ['', ''],# freezing drizzle
	'284': ['', ''],# heavy freezing rain
	'293': ['', ''],# patchy light rain
	'296': ['', ''],# light rain
	'299': ['', ''],# patchy moderate rain
	'302': ['', ''],# moderate rain
	'305': ['', ''],# patchy heavy rain
	'308': ['', ''],# heavy rain
	'311': ['', ''],# light freezing rain
	'314': ['', ''],# moderate or heavy freezing rain
	'317': ['', ''],# light sleet
	'320': ['', ''],# moderate or heavy sleet
	'323': ['', ''],# patchy light snow
	'326': ['', ''],# light snow
	'329': ['', ''],# patchy moderate snow
	'332': ['', ''],# moderate snow
	'335': ['', ''],# patchy heavy snow
	'338': ['', ''],# heavy snow
	'350': ['', ''],# hail
	'353': ['', ''],# light rain shower
	'356': ['', ''],# moderate or heavy rain showers
	'359': ['', ''],# torrential rain showers
	'362': ['', ''],# light sleet showers
	'365': ['', ''],# moderate or heavy sleet showers
	'368': ['', ''],# light snow showers
	'371': ['', ''],# moderate or heavy snow showers
	'374': ['', ''],# light hail
	'377': ['', ''],# moderate or heavy hail
	'386': ['', ''],# patchy light rain in area with thunder
	'389': ['', ''],# moderate or heavy rain in area with thunder
	'392': ['', ''],# patchy light snow in area with thunder
	'395': ['', ''],# moderate or heavy snow in area with thunder
}

def get_weather_data(max_retries=32, retry_delay=1):
	retries = 0
	while retries < max_retries:
		try:
			return requests.get("https://wttr.in/?format=j2").json()
		except requests.RequestException as e:
			retries += 1
			time.sleep(retry_delay)  # Wait before retrying
	return None  # Return None after exhausting retries

weather = get_weather_data()
if weather:
	current_condition = weather['current_condition'][0]
	weather_code = current_condition['weatherCode']
	astronomy = weather['weather'][0]['astronomy'][0]

	try:
		sunrise = datetime.strptime(astronomy['sunrise'], "%I:%M %p").time()
		sunset = datetime.strptime(astronomy['sunset'], "%I:%M %p").time()
	except KeyError:
		exit(1)

	now = datetime.now().time()
	is_night = int(now < sunrise or now > sunset)

	data = {}
	if weather_code in WEATHER_CODES:
		# if there's only one icon for the weather code, duplicate it for day and night display
		if len(WEATHER_CODES[weather_code]) == 1:
			WEATHER_CODES[weather_code] = [WEATHER_CODES[weather_code][0]] * 2
		data['text'] = WEATHER_CODES.get(weather_code, ['?', '?'])[is_night] + f" {current_condition['temp_F']}°F"
	else:
		data['text'] = f"? {current_condition['temp_F']}°F"

	# ... (Enriched tooltip information)
	data['tooltip'] = f"{current_condition['weatherDesc'][0]['value']}\n"
	data['tooltip'] += f"\t{current_condition['FeelsLikeF']}°F\n"
	data['tooltip'] += f"\t{current_condition['humidity']}%\n"
	data['tooltip'] += f"\t{current_condition['winddir16Point']} {current_condition['windspeedMiles']} mph\n"
	data['tooltip'] += f"󰡵\t{current_condition['pressure']} inHg"

# show moon phase eventually

print(json.dumps(data))