# Hyprgnome

## Dependencies
[GNOME](https://wiki.archlinux.org/title/GNOME)

[Hyprland](https://hyprland.org/)

### Arch Linux
```bash
FONTS="inter-font ttf-fira-code ttf-firacode-nerd"
SYSTEM="chromium jq pamixer pipewire wireplumber wl-clipboard xorg-xrandr"
GNOME="gnome polkit-gnome network-manager-applet nwg-look zenity"
HYPR="hyprland dunst qt5-wayland qt6-wayland qt5ct qt6ct xdg-desktop-portal-hyprland"
MISC="brightnessctl blueman hypridle hyprlock libqalculate grim slurp tesseract waybar"
sudo pacman -Syu $FONTS $SYSTEM $GNOME $HYPR $MISC
```

## Usage

### Clone the repository

`git clone https://gitlab.com/kohltastrophe/hyprgnome.git`

### Symlink the required files

`ln -rs hyprgnome/.config/* ~/.config/`

*Make sure there are no conflicting folders in `~/.config/`*

### Configuration

**Check out [Hyprland](https://wiki.hyprland.org/Getting-Started/Master-Tutorial/) and the `hyprgnome/.config/hypr/hyprland.conf` file for more information.**
